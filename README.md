The structure of the project is as follows:
Section 1 covers the design of the combinational components which are utilised in the crypto-processor.
Section 2 covers synchronous designs, as well as the components needed to ensure the data processed by our combinational logic can be stored.
Finally a test program is given to ensure the final synchronous design works as expected