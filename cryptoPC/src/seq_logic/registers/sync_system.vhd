----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.11.2018 22:55:15
-- Design Name: 
-- Module Name: sync_system - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

entity sync_system is
    Port ( ctrl : in STD_LOGIC_VECTOR (3 downto 0):=(others=>'0');
           aBus : in STD_LOGIC_VECTOR (15 downto 0):=(others=>'0');
           bBus : in STD_LOGIC_VECTOR (15 downto 0):=(others=>'0');
           clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           result : out STD_LOGIC_VECTOR (15 downto 0);
           async_result : out STD_LOGIC_VECTOR (15 downto 0)
           );
end sync_system;

architecture rtl of sync_system is

component logicSyst is
    Port ( aBus : in STD_LOGIC_VECTOR (15 downto 0);
           bBus : in STD_LOGIC_VECTOR (15 downto 0);
           ctrlBus : in STD_LOGIC_VECTOR (3 downto 0);
           result : out STD_LOGIC_VECTOR (15 downto 0));
end component;

signal ctrl_r: std_logic_vector(3 downto 0);
signal aBus_r: std_logic_vector(15 downto 0);
signal bBus_r: std_logic_vector(15 downto 0);
signal result_int: std_logic_vector(15 downto 0);

begin
    async_result <= result_int;         -- Async output of the logic for internal usage
    
    input_reg: process (ctrl,aBus,bBus,clk,reset) is
        begin 
            if (reset='1') then
                ctrl_r <= (others=>'0');
                aBus_r <= (others=>'0');
                bBus_r <= (others=>'0');
            elsif (rising_edge(clk)) then
                ctrl_r <= ctrl;
                aBus_r <= aBus;
                bBus_r <= bBus;
            end if;
    end process;    
    
    output_reg: process (ctrl,aBus,bBus,clk,reset) is
            begin 
                if (reset='1') then
                    result <= (others=>'0');
                elsif (falling_edge(clk)) then
                    result <= result_int;
                end if;
         end process;
 comb_system : logicSyst port map (aBus=>aBus_r,bBus=>bBus_r,ctrlBus=>ctrl_r,result=>result_int);

end rtl;
