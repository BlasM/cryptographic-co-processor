library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity register_file is
port(
	aBus: out std_logic_vector(15 downto 0);
	bBus: out std_logic_vector(15 downto 0);
	result: in std_logic_vector(15 downto 0):=(others=>'0');
	we: in std_logic:='1';
	regASel: in std_logic_vector(3 downto 0):=(others=>'0');
	regBSel: in std_logic_vector(3 downto 0):=(others=>'0');
	wRegSel: in std_logic_vector(3 downto 0):=(others=>'0');
	reset: in std_logic := '0';
	clk: in std_logic :='0'
);
end entity;

------ Sync Memory architecture for RD & WR --------
architecture sync_wr of register_file is

type memory is array (0 to 15) of std_logic_vector(15 downto 0);
signal reg_file : memory:= (
	0 => x"0001",
	1 => x"c505",
	2 => x"3c07",
	3 => x"d405",
	4 => x"1186",
	5 => x"f407",
	6 => x"1086",
	7 => x"4706",
	8 => x"6808",
	9 => x"baa0",
	10 => x"c902",
	11 => x"100b",
	12 => x"c000",
	13 => x"c902",
	14 => x"100b",
	15 => x"b000",
	others => (others=>'0')
);  
begin
syn_ram_write_read : process (clk,reset,result,regASel,regBSel,we)
begin
	if (reset = '1') then 
		aBus<=(others=>'0');
		bBus<=(others=>'0');
		-- init memory with default values after reset
--		reg_file <=(
--		            x"0001",x"c505",x"3c07",x"d405",x"1186",x"f407",
--                    x"1086",x"4706",x"6808",x"baa0",x"c902",x"100b",
--                    x"c000",x"c902",x"100b",x"b000"
--		);
	elsif (rising_edge(clk)) then
		if (we='1') then
			reg_file(to_integer(unsigned(wRegSel)))<= result;    --- Write in memory with Write_enable = '1'
			if (wRegSel = regASel) then                          --- If Input address from Reg A is same than writen addresss, pass on to exit
				aBus <= result;
				bBus <= reg_file(to_integer(unsigned(regBSel)));
			elsif (wRegSel = regBSel) then                       --- If Input address from Reg B is same than writen addresss, pass on to exit
				bBus <= result;
				aBus <= reg_file(to_integer(unsigned(regASel)));
			end if;
		else
			aBus <= reg_file(to_integer(unsigned(regASel)));     --- If not, then use the address from the Regs
			bBus <= reg_file(to_integer(unsigned(regBSel)));
		end if;
	end if;
end process syn_ram_write_read;
end sync_wr;

--- End of Sync Memory architecture --------


------ Async Memory architecture for Read and Sync for Write with falling clock edge --------
architecture asyncr_syncw of register_file is

type memory is array (0 to 15) of std_logic_vector(15 downto 0);
signal reg_file : memory:= (
	0 => x"0001",
	1 => x"c505",
	2 => x"3c07",
	3 => x"d405",
	4 => x"1186",
	5 => x"f407",
	6 => x"1086",
	7 => x"4706",
	8 => x"6808",
	9 => x"baa0",
	10 => x"c902",
	11 => x"100b",
	12 => x"c000",
	13 => x"c902",
	14 => x"100b",
	15 => x"b000",
	others => (others=>'0')
);  
begin
async_ram_read : process (clk,reset,result,regASel,regBSel,we)
begin
	if (reset = '1') then 
		aBus<=(others=>'0');
		bBus<=(others=>'0');
		-- init memory with default values after reset
--		reg_file <=(
--		            x"0001",x"c505",x"3c07",x"d405",x"1186",x"f407",
--                    x"1086",x"4706",x"6808",x"baa0",x"c902",x"100b",
--                    x"c000",x"c902",x"100b",x"b000"
--		);
	elsif (falling_edge(clk)) then                             --- write sync with rising/falling edge
		if (we='1') then
			reg_file(to_integer(unsigned(wRegSel)))<= result;
	    end if;
	end if;
	aBus <= reg_file(to_integer(unsigned(regASel)));           --- Async Read process
    bBus <= reg_file(to_integer(unsigned(regBSel)));
end process async_ram_read;
end asyncr_syncw;
------ End of Async Memory architecture with RD & Syc-Lclock WR --------
