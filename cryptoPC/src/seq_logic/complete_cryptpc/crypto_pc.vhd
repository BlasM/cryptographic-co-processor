----------------------------------------------------------------------------------
-- Company: DCU
-- Engineer: Blas Molina
-- 
-- Create Date: 27.11.2018 17:58:38
-- Design Name: 
-- Module Name: crypto_pc - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library xil_defaultlib;
use xil_defaultlib.all;


entity crypto_pc is
  Port ( 
    reset: in std_logic := '0';
    clk: in std_logic :='0';
    instWord: in std_logic_vector(15 downto 0):=(others=>'0');
    result: out std_logic_vector(15 downto 0):=(others=>'0')
);
end crypto_pc;

architecture rtl of crypto_pc is

component sync_system is
    Port ( ctrl : in STD_LOGIC_VECTOR (3 downto 0);
           aBus : in STD_LOGIC_VECTOR (15 downto 0);
           bBus : in STD_LOGIC_VECTOR (15 downto 0);
           clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           result : out STD_LOGIC_VECTOR (15 downto 0);
           async_result : out STD_LOGIC_VECTOR (15 downto 0));
end component;

component register_file is
port(
	aBus: out std_logic_vector(15 downto 0);
	bBus: out std_logic_vector(15 downto 0);
	result: in std_logic_vector(15 downto 0);
	we: in std_logic;
	regASel: in std_logic_vector(3 downto 0);
	regBSel: in std_logic_vector(3 downto 0);
	wRegSel: in std_logic_vector(3 downto 0);
	reset: in std_logic;
	clk: in std_logic
);
end component;

signal result_pc, aBus_pc, bBus_pc, result_int : std_logic_vector(15 downto 0);
signal we_pc : std_logic;

begin
 
 we_pc<=  instWord(15) or not(instWord(14)) or not(instWord(13)) or not(instWord(12)); -- NOP control always '1' but 0111 = '0'
 result <= result_pc;       -- externalizing the results for Synthesis process

 system : sync_system port map (
                        clk=>clk,
                        reset=>reset,
                        aBus=>aBus_pc,
                        bBus=>bBus_pc,
                        ctrl=>instWord(15 downto 12),
                        result=>result_pc,
                        async_result=>result_int
 );
 memory : entity xil_defaultlib.register_file(asyncr_syncw) port map ---- old arch sync_wr
                (
                        clk=>clk,
                        reset=>reset,
                        we=>we_pc,
                        regASel=>instWord(11 downto 8),
                        regBSel=>instWord(7 downto 4),
                        wRegSel=>instWord(3 downto 0),
                        aBus=>aBus_pc,
                        bBus=>bBus_pc,
                        result=>result_int
 );
 
end rtl;
