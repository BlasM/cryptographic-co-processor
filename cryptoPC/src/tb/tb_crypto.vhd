library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.textio.all;
use ieee.std_logic_textio.all;

--use work.all;
library xil_defaultlib;
use xil_defaultlib.SystPkg.all;
use xil_defaultlib.all;
 
entity tb_crypto is
end tb_crypto;
 
architecture vector_test of tb_crypto is
constant PERIOD: time := 20 ns;
signal clk, reset : std_logic := '0';
signal c_line: integer := 0;
signal instWord_line : std_logic_vector(15 downto 0):=(others=>'0');
signal result, reg_expect : std_logic_vector(15 downto 0); 


component crypto_pc is
  Port ( 
    reset: in std_logic;
    clk: in std_logic;
    instWord: in std_logic_vector(15 downto 0);
    result: out std_logic_vector(15 downto 0)
);
end component;

 
begin
    UUT: crypto_pc port map (
                            reset=>reset,
							clk=>clk,
							instWord=>instWord_line,
							result=>result
							);
    
   clock_signal : process(clk) 
    begin 
        clk <= not clk after PERIOD/2;
    end process clock_signal;   
 
	readcmd: process
 
        -- This process loops through a file and reads one line
        -- at a time, parsing the line to get the values and
        -- expected result.
        file vectors,results: TEXT;       -- Define the file 'handle'
        variable line_in,line_out: Line; -- Line buffers
        variable good: boolean;   -- Status of the read operations
		-- data format from the Vectors File 
        variable cline: integer;
        variable r_expect: std_logic_vector(15 downto 0); 
		variable instWord: std_logic_vector(15 downto 0);
        constant TEST_PASSED: string := "PASS";
        constant TEST_FAILED: string := "FAIL";
  
	begin
        
        wait for (PERIOD/4); ---- wait for 3 and half periods
        
        file_open(vectors,"cryptoHex.txt",read_mode);
        file_open(results,"resCryptoHex.txt",write_mode);
 
	while not endfile(vectors) loop
		readline(vectors,line_in);     -- Read a line from the file
		next when line_in'length = 0;  -- Skip empty lines
 
		read(line_in,cline,good);     -- Read line number input
	    assert good
	       report "Text I/O read error with Line number"
	       severity ERROR;

		hread(line_in,instWord,good);     -- Read the Instruction Word argument as hex value
		assert good
	       report "Text I/O read error at Instruction Word"
	       severity ERROR;
 
		-- hread(line_in,ra,good);     -- Read the A argument as hex value
		-- assert good
	        -- report "Text I/O read error at RA"
	        -- severity ERROR;
 
        -- hread(line_in,rb,good);     -- Read the B argument
	    -- assert good
	         -- report "Text I/O read error at RB"
	         -- severity ERROR;
 
         hread(line_in,r_expect,good);     -- Read the expected resulted
	     assert good
	          report "Text I/O read error at R_Expected"
	          severity ERROR;
 
 		c_line <= cline;
 		instWord_line <= instWord;
 		reg_expect <= r_expect;
 
        wait for (PERIOD);   -- Give the circuit time to stabilize
 
         if (result /= r_expect) then
             report "Line: " & integer'image(cline) & " OpCode:  " & integer'image(to_integer(unsigned(instWord))) & " " & TEST_FAILED; 
         else
             report "Line: " & integer'image(cline) & " " & TEST_PASSED;
         end if;
             --- Write values to results file ---
             write(line_out,cline,RIGHT,3);
             hwrite(line_out,result,RIGHT,5);
             hwrite(line_out,r_expect,RIGHT,5);
             writeline(results,line_out);     -- write the message
 
	end loop;
	file_close(vectors);
	file_close(results);
	assert false
        	report "End of file encountered;"
            severity NOTE;
    wait;
 	end process;
end architecture vector_test;

----------------------------------------------------------------------------
------ Architecture with the Package for reding Register FIles  ------------
---------------------------------------------------------------------------
--architecture register_test of tb_crypto is
--constant PERIOD: time := 20 ns;
--signal clk, reset : std_logic := '0';
--signal c_line: integer := 0;
--signal instWord_line : std_logic_vector(15 downto 0):=(others=>'0');
--signal result, reg_expect : std_logic_vector(15 downto 0); 


--component crypto_pc is
--  Port ( 
--    reset: in std_logic;
--    clk: in std_logic;
--    instWord: in std_logic_vector(15 downto 0);
--    result: out std_logic_vector(15 downto 0)
--);
--end component;

 
--begin
--    UUT: crypto_pc port map (
--                            reset=>reset,
--							clk=>clk,
--							instWord=>instWord_line,
--							result=>result
--							);
    
--   clock_signal : process(clk) 
--    begin 
--        clk <= not clk after PERIOD/2;
--    end process clock_signal;   
 
--	readcmd: process
 
--        -- This process loops through a file and reads one line
--        -- at a time, parsing the line to get the values and
--        -- expected result.
--        file vectors,results: TEXT;       -- Define the file 'handle'
--        variable line_in,line_out: Line; -- Line buffers
--        variable good: boolean;   -- Status of the read operations
--		-- data format from the Vectors File 
--        variable cline: integer;
        
--        variable opCode_in: string(1 to 3);
--        variable r, space : character;
        
--        variable ra_num, rb_num, r_mem : integer;
        
--        variable r_expect: std_logic_vector(15 downto 0); 
--		--variable instWord: std_logic_vector(15 downto 0);
--        constant TEST_PASSED: string := "PASS";
--        constant TEST_FAILED: string := "FAIL";
  
--	begin
        
--        wait for (PERIOD/4); ---- wait for 1/4 of a period
        
--        file_open(vectors,"test_prog.txt",read_mode);
--        file_open(results,"resCryptoProg.txt",write_mode);
 
--	while not endfile(vectors) loop
--		readline(vectors,line_in);     -- Read a line from the file
--		next when line_in'length = 0;  -- Skip empty lines
 
--		read(line_in,cline,good);     -- Read line number input
--	    assert good
--	       report "Text I/O read error with Line number"
--	       severity ERROR;
--		read(line_in,space,good);     -- Read line number input
--        assert good
--            report "Text I/O read error with blank space character"
--            severity ERROR;
--        read(line_in,opCode_in,good);     -- Read line number input
--        assert good
--            report "Text I/O read error with OpCode command"
--            severity ERROR;
--        read(line_in,space,good);     -- Read line number input
--        assert good
--            report "Text I/O read error with blank space character"
--            severity ERROR;
--        read(line_in,r,good);     -- Read line number input
--        assert good
--            report "Text I/O read error with Register character"
--            severity ERROR;
--        read(line_in,ra_num,good);     -- Read line number input
--        assert good
--            report "Text I/O read error with Register_A Number"
--            severity ERROR;
--        read(line_in,space,good);     -- Read line number input
--        assert good
--            report "Text I/O read error with blank space character"
--            severity ERROR;
--        read(line_in,r,good);     -- Read line number input
--        assert good
--            report "Text I/O read error with Register character"
--            severity ERROR;
--        read(line_in,rb_num,good);     -- Read line number input
--        assert good
--            report "Text I/O read error with Register_B Number"
--            severity ERROR;
--        read(line_in,space,good);     -- Read line number input
--        assert good
--            report "Text I/O read error with blank space character"
--            severity ERROR;
--        read(line_in,r,good);     -- Read line number input
--        assert good
--            report "Text I/O read error with Register character"
--            severity ERROR;
--        read(line_in,r_mem,good);     -- Read line number input
--        assert good
--            report "Text I/O read error with Memory Register Number"
--            severity ERROR;
--        read(line_in,space,good);     -- Read line number input
--        assert good
--            report "Text I/O read error with blank space character"
--            severity ERROR;
--        hread(line_in,r_expect,good);     -- Read the expected result from the register program file as hex value
--		assert good
--	       report "Text I/O read error at Expected result value"
--	       severity ERROR;
--        --- Signals assignation ---
-- 		c_line <= cline;
-- 		instWord_line <= OpCode2Std(opCode_in) & Int2Std(ra_num,4) & Int2Std(rb_num,4) & Int2Std(r_mem,4);
--        reg_expect <= r_expect;
 
--        wait for (PERIOD);   -- Give the circuit time to stabilize
 
--         if (result /= r_expect) then
--             report "Line: " & integer'image(cline) & " OpCode:  " & opCode_in & " " & TEST_FAILED; 
--         else
--             report "Line: " & integer'image(cline) & " " & TEST_PASSED;
--         end if;
--             --- Write values to results file ---
--             write(line_out,cline,RIGHT,3);
--             hwrite(line_out,result,RIGHT,5);
--             hwrite(line_out,r_expect,RIGHT,5);
--             writeline(results,line_out);     -- write the message
 
--	end loop;
--	file_close(vectors);
--	file_close(results);
--	assert false
--        	report "End of file encountered;"
--            severity NOTE;
--    wait;
-- 	end process;
--end architecture register_test;