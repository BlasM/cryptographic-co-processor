library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.textio.all;
use ieee.std_logic_textio.all;

--use work.all;
library xil_defaultlib;
use xil_defaultlib.all;
 
entity tb_StrDesign is
end tb_StrDesign;
 
architecture readhex of tb_StrDesign is
constant PERIOD: time := 10 ns;
signal c_line: integer := 0;
signal op_Code: std_logic_vector(3 downto 0) := (others => '0');
signal reg_a, reg_b, result, reg_expect : std_logic_vector(15 downto 0) := (others => '0'); 

component logicSyst is
    Port ( aBus : in STD_LOGIC_VECTOR (15 downto 0);
           bBus : in STD_LOGIC_VECTOR (15 downto 0);
           ctrlBus : in STD_LOGIC_VECTOR (3 downto 0);
           result : out STD_LOGIC_VECTOR (15 downto 0));
end component;

 
begin
    UUT: logicSyst port map (aBus=>reg_a,bBus=>reg_b,ctrlBus=>op_Code,result=>result);
 
	readcmd: process
 
        -- This process loops through a file and reads one line
        -- at a time, parsing the line to get the values and
        -- expected result.
        file vectors,results: TEXT;       -- Define the file 'handle'
        variable line_in,line_out: Line; -- Line buffers
        variable good: boolean;   -- Status of the read operations
 
        variable cline: integer;
        variable ra,rb,r_expect: std_logic_vector(15 downto 0); 
        variable opCode: std_logic_vector(3 downto 0); 
        constant TEST_PASSED: string := "PASS";
        constant TEST_FAILED: string := "FAIL";
  
	begin
 
        file_open(vectors,"testVectHex.txt",read_mode);
        file_open(results,"resultsHex.txt",write_mode);
 
	while not endfile(vectors) loop
		readline(vectors,line_in);     -- Read a line from the file
		next when line_in'length = 0;  -- Skip empty lines
 
		read(line_in,cline,good);     -- Read line number input
	    assert good
	       report "Text I/O read error with Line number"
	       severity ERROR;

		hread(line_in,opCOde,good);     -- Read the opCode argument as hex value
		assert good
	       report "Text I/O read error at opCode"
	       severity ERROR;
 
		hread(line_in,ra,good);     -- Read the A argument as hex value
		assert good
	        report "Text I/O read error at RA"
	        severity ERROR;
 
        hread(line_in,rb,good);     -- Read the B argument
	    assert good
	         report "Text I/O read error at RB"
	         severity ERROR;
 
        hread(line_in,r_expect,good);     -- Read the expected resulted
	    assert good
	         report "Text I/O read error at R_Expected"
	         severity ERROR;
 
 		c_line <= cline;
 		op_Code <= opCode;
	    reg_a <= ra;
	    reg_b <= rb;
		reg_expect <= r_expect;
 
        wait for PERIOD;   -- Give the circuit time to stabilize
 
        if (result /= r_expect) then
            report "Line: " & integer'image(cline) & " OpCode:  " & integer'image(to_integer(unsigned(opCode))) & " " & TEST_FAILED; 
        else
            report "Line: " & integer'image(cline) & " " & TEST_PASSED;
        end if;
            -- Write values to results file ---
            write(line_out,cline,RIGHT,3);
            hwrite(line_out,ra,RIGHT,5);
            hwrite(line_out,rb,RIGHT,5);
            hwrite(line_out,result,RIGHT,5);
            hwrite(line_out,r_expect,RIGHT,5);
            writeline(results,line_out);     -- write the message
 
	end loop;
	file_close(vectors);
	file_close(results);
	assert false
        	report "End of file encountered;"
            severity NOTE;
    wait;
 	end process;
end architecture readhex;