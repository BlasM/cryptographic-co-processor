library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package SystPkg is 

function OpCode2Std (op: string(1 to 3)) return std_logic_vector;
function Int2Std (num: integer; nbits: integer) return std_logic_vector;

end SystPkg;

package body SystPkg is

	function OpCode2Std (op: string(1 to 3)) return std_logic_vector is
	variable op_std : std_logic_vector(3 downto 0);
	begin
		--process(op)
		--begin 
			case (op) is
				when "ADD" => op_std :="0000";
				when "SUB" => op_std :="0001";
				when "AND" => op_std :="0010";
				when "ORR" => op_std :="0011";
				when "XOR" => op_std :="0100";
				when "NOT" => op_std :="0101";
				when "MOV" => op_std :="0110";
				when "ROR" => op_std :="1000";
				when "ROL" => op_std :="1001";
				when "SLL" => op_std :="1010";
				when "SRL" => op_std :="1011";
				when "LUT" => op_std :="1100";
				when "NOP" => op_std :="0111";
				when others => op_std :="0111";		-- NOP when undefined operation
			end case;
		return op_std;		
	end OpCode2Std;
	
	function Int2Std (num: integer; nbits: integer) return std_logic_vector is
	begin
		return (std_logic_vector(to_unsigned(num , nbits)));
	end Int2Std;
end SystPkg;