`timescale 1ns / 1ns

module fullAdder(a,b,c_in,s,c_out);
input a,b,c_in;
output s,c_out;

wire sig1,sig2,sig3;

assign sig1 = a ^ b;
assign s = sig1 ^ c_in; // Adder output value

assign sig2 = a & b;
assign sig3 = sig1 & c_in;
assign c_out = sig3 | sig2; // Output Carry value

endmodule
