`timescale 1ns / 100ps

module nAdder 
#(parameter N=16)//Number of Adders by default
(
input [N-1:0] a,b,
input c_in,
output [N-1:0] s,
output c_out
);

wire [N:0] intC_in;
assign intC_in[0] = c_in;
assign c_out = intC_in[N]; 

genvar num;
generate
    for(num=0; num<N; num=num+1) 
	begin: generate_block_nAdder
		fullAdder nAdder(
			.a(a[num]),
			.b(b[num]),
			.c_in(intC_in[num]),
			.s(s[num]),
			.c_out(intC_in[num+1]));
	end
endgenerate
endmodule