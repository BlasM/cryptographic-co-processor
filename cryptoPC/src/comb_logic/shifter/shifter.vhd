library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity shifter is
generic (N: natural := 3);  --- default number of input bits
port (
	shiftIn: in std_logic_vector(N-1 downto 0);
	shiftCtrl: in std_logic_vector(1 downto 0);
	shiftOut: out std_logic_vector(N-1 downto 0)
);
end shifter;


architecture rtl of shifter is
constant SBITS : natural := 4; --- Number of Bits to shift
begin
	process(shiftIn,shiftCtrl)
	variable tmpBits: std_logic_vector(SBITS-1 downto 0);
	begin
	case (shiftCtrl) is
		when "00" => shiftOut<=to_stdlogicvector(to_bitvector(shiftIn) ror SBITS);
		when "01" => shiftOut<=to_stdlogicvector(to_bitvector(shiftIn) rol SBITS);
		when "10" => shiftOut<=to_stdlogicvector(to_bitvector(shiftIn) sll SBITS);
		when "11" => shiftOut<=to_stdlogicvector(to_bitvector(shiftIn) srl SBITS);
		when others => shiftOut<=shiftIn;
	end case;
	end process;

end rtl;