library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sBox2 is
port(
	sBoxIn: in std_logic_vector(3 downto 0);
	sBoxOut: out std_logic_vector(3 downto 0)
);
end entity;

architecture rtl of sBox2 is
begin
sBox2: process (sBoxIn)
	begin
	case (sBoxIn) is
	   when "0000" => sBoxOut <= "1111";    -- 0 --> 15
	   when "0001" => sBoxOut <= "0000";    -- 1 --> 0
	   when "0010" => sBoxOut <= "1101";    -- 2 --> 13;
	   when "0011" => sBoxOut <= "0111";    -- 3 --> 7;
	   when "0100" => sBoxOut <= "1011";    -- 4 --> 11;
	   when "0101" => sBoxOut <= "1110";    -- 5 --> 14;
	   when "0110" => sBoxOut <= "0101";    -- 6 --> 5;
       when "0111" => sBoxOut <= "1010";    -- 7 --> 10;
	   when "1000" => sBoxOut <= "1001";    -- 8 --> 9;
	   when "1001" => sBoxOut <= "0010";    -- 9 --> 2
	   when "1010" => sBoxOut <= "1100";    -- 10 --> 12;
	   when "1011" => sBoxOut <= "0001";    -- 11 --> 1;
	   when "1100" => sBoxOut <= "0011";    -- 12 --> 3;
	   when "1101" => sBoxOut <= "0100";    -- 13 --> 4;
	   when "1110" => sBoxOut <= "1000";    -- 14 --> 8;
	   when "1111" => sBoxOut <= "0110";    -- 15 --> 6;
	   when others => sBoxOut <= sBoxIn;
	end case;
end process sBox2; 
end architecture;