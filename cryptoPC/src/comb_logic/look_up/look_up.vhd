library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity look_up is
generic (N : natural := 16);
port(
	lutIn: in std_logic_vector(N-1 downto 0);
	lutEn: in std_logic;
	lutOut: out std_logic_vector(N-1 downto 0)
);
end entity;

architecture rtl of look_up is

signal sBoxOut1 : std_logic_vector(3 downto 0); 
signal sBoxOut2 : std_logic_vector(3 downto 0);

component sBox1 is
port(
	sBoxIn: in std_logic_vector(3 downto 0);
	sBoxOut: out std_logic_vector(3 downto 0)
);
end component;

component sBox2 is
port(
	sBoxIn: in std_logic_vector(3 downto 0);
	sBoxOut: out std_logic_vector(3 downto 0)
);
end component;


begin
EnLook_Table: process (lutEn,lutIn,sBoxOut1,sBoxOut2) --- Codification enable with lutEn, otherwise bypass
	begin
	if (lutEn = '1') then
	   lutOut <= lutIn(N-1 downto 8)& sBoxOut1 & sBoxOut2;
	else
	   lutOut <= (others=>'0');
	end if;
end process EnLook_Table;

sBox1_Inst : sBox1 port map(sBoxIn=>lutIn(7 downto 4),sBoxOut=>sBoxOut1);
sBox2_Inst : sBox2 port map(sBoxIn=>lutIn(3 downto 0),sBoxOut=>sBoxOut2);

end architecture;