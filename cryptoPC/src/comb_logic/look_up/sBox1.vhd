library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sBox1 is
port(
	sBoxIn: in std_logic_vector(3 downto 0);
	sBoxOut: out std_logic_vector(3 downto 0)
);
end entity;

architecture rtl of sBox1 is
begin
sBox1: process (sBoxIn)
	begin
	case (sBoxIn) is
	   when "0000" => sBoxOut <="0001";    -- 0 --> 1
	   when "0001" => sBoxOut <= "1011";     -- 1 --> 11
	   when "0010" => sBoxOut <= "1001";    -- 2 --> 9;
	   when "0011" => sBoxOut <= "1100";     -- 3 --> 12;
	   when "0100" => sBoxOut <= "1101";     -- 4 --> 13;
	   when "0101" => sBoxOut <= "0110";     -- 5 --> 6;
	   when "0110" => sBoxOut <= "1111";     -- 6 --> 15;
       when "0111" => sBoxOut <= "0011";     -- 7 --> 3;
	   when "1000" => sBoxOut <= "1110";     -- 8 --> 14;
	   when "1001" => sBoxOut <= "1000";     -- 9 --> 8
	   when "1010" => sBoxOut <= "0111";     -- 10 --> 7;
	   when "1011" => sBoxOut <= "0100";     -- 11 --> 4;
	   when "1100" => sBoxOut <= "1010";     -- 12 --> 10;
	   when "1101" => sBoxOut <= "0010";     -- 13 --> 2;
	   when "1110" => sBoxOut <= "0101";     -- 14 --> 5;
	   when "1111" => sBoxOut <= "0000";     -- 15 --> 0;
	   when others => sBoxOut <= sBoxIn;
	end case;
end process sBox1; 
end architecture;