library ieee;
use ieee.std_logic_1164.all;

entity alu is
generic ( N : natural := 16); -- default value of alu modules 
port (
	aBus: in std_logic_vector(N-1 downto 0);
	bBus: in std_logic_vector(N-1 downto 0);
	aluControl: in std_logic_vector(2 downto 0);
	aluOut: out std_logic_vector(N-1 downto 0)
);
end entity alu;

architecture structural of alu is
component nAdder
	generic (N: natural );
	port(	
	   a,b: in std_logic_vector(N-1 downto 0);
	   c_in: in std_logic;
	   s: out std_logic_vector(N-1 downto 0);
	   c_out: out std_logic
	);
end component nAdder;

signal intbBus: std_logic_vector(N-1 downto 0);
signal intAddSub: std_logic_vector(N-1 downto 0);
signal intCin: std_logic;

begin
Mux_alu : process(bBus,aluControl(0))   -- Process to control if Addition or Substraction is carried based in the input bBus and the LSB from the crl signal
    begin
    case (aluControl(0)) is
        when '0'=>  intbBus <= bBus;    --- Addition set-up
                    intCin <= '0';
        when '1'=>  intbBus <= not(bBus);   --- Substraction set-up: (~B) + 1 for Adder operations 
                    intCin <= '1';
        when others=> null;
    end case;
 end process Mux_alu;

Mux_Inst:	process(aBus,bBus,aluControl,intAddSub)
	begin
	case(aluControl) is 
		when "000" => aluOut <= intAddSub; --- Addition/Substraction process from Adder unit			
		when "001" => aluOut <= intAddSub;
		when "010" => aluOut <= aBus and bBus; --- AND Oper
		when "011" => aluOut <= aBus or bBus; --- OR Oper
		when "100" => aluOut <= aBus xor bBus;-- XOR Oper
		when "101" => aluOut <= not aBus;-- NOT Oper
		when "110" => aluOut <= aBus; -- MOV Oper
		when others => aluOut <= (others=>'0');			
	end case;
end process Mux_Inst;

	--- Adder instantiation ---
	IntAdder: nAdder generic map(N=>N) port map(a=>aBus,b=>intbBus,c_in=>intCin,s=>intAddSub,c_out=>open);
end structural;