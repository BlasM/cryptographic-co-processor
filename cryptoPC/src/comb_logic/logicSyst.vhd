library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity logicSyst is
    Port ( aBus : in STD_LOGIC_VECTOR (15 downto 0);
           bBus : in STD_LOGIC_VECTOR (15 downto 0);
           ctrlBus : in STD_LOGIC_VECTOR (3 downto 0);
           result : out STD_LOGIC_VECTOR (15 downto 0)
           );
end logicSyst;

architecture struct of logicSyst is
signal resultCtrl: std_logic_vector(1 downto 0):="11";
signal aluCtrl: std_logic_vector (2 downto 0);
signal shiftCtrl: std_logic_vector (1 downto 0);
signal lutEn: std_logic;
signal lutOut: std_logic_vector(15 downto 0);
signal aluOut: std_logic_vector(15 downto 0);
signal shiftOut: std_logic_vector(15 downto 0);

component look_up is
generic (N : natural := 16);
port(
	lutIn: in std_logic_vector(N-1 downto 0);
	lutEn: in std_logic;
	lutOut: out std_logic_vector(N-1 downto 0)
);
end component;

component alu is
generic ( N : natural := 16); 
port (
	aBus: in std_logic_vector(N-1 downto 0);
	bBus: in std_logic_vector(N-1 downto 0);
	aluControl: in std_logic_vector(2 downto 0);
	aluOut: out std_logic_vector(N-1 downto 0)
);
end component;

component ctrlUnit is
  Port (
        codeCtrl: in  std_logic_vector (3 downto 0);
        aluCtrl: out  std_logic_vector (2 downto 0);
        shiftCtrl: out  std_logic_vector (1 downto 0);
        lutEn: out  std_logic;
        resultCtrl : out std_logic_vector(1 downto 0));
end component;

component shifter is
generic (N: natural := 16);
port (
	shiftIn: in std_logic_vector(N-1 downto 0);
	shiftCtrl: in std_logic_vector(1 downto 0);
	shiftOut: out std_logic_vector(N-1 downto 0)
);
end component;

begin

Control_UnitInst: ctrlUnit port map (codeCtrl=>ctrlBus,aluCtrl=>aluCtrl,shiftCtrl=>shiftCtrl,lutEn=>lutEn,resultCtrl=>resultCtrl);
Look_upInst : look_up generic map (N => 16) port map (lutIn=>aBus,lutEn=>lutEn,lutOut=>lutOut);
ALU_Inst : alu generic map (N =>16) port map (aBus=>aBus,bBus=>bBus,aluControl=>aluCtrl,aluOut=>aluOut);
Shifter_Inst:  shifter generic map (N=> 16) port map(shiftIn=>bBus,shiftCtrl=>shiftCtrl,shiftOut=>shiftOut);

mux_Syst : process(resultCtrl,lutOut,aluOut,shiftOut)   --- output Multiplexer to select correct output from Comb System
begin
    case(resultCtrl) is 
        when "00" => result<=lutOut;
        when "01" => result<=aluOut;
        when "10" => result<=shiftOut;
        when "11" => result<=(others=>'0'); --- default GND
        when others => result<=(others=>'0'); 
    end case;
end process mux_Syst;



end struct;
