library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ctrlUnit is
  Port (
        codeCtrl: in  std_logic_vector (3 downto 0);
        aluCtrl: out  std_logic_vector (2 downto 0);
        shiftCtrl: out  std_logic_vector (1 downto 0);
        lutEn: out  std_logic;
        resultCtrl : out std_logic_vector(1 downto 0));
end ctrlUnit;

architecture rtl of ctrlUnit is
begin

ctrl_Unit: process (codeCtrl)
	begin
		if (codeCtrl(3)= '0') then    -- Code cotrol for the ALU unit, enabling ALU output only from output Multiplexer : result = 01
    	   aluCtrl <= codeCtrl(2 downto 0);
    	   shiftCtrl <= "00";
           lutEn <= '0';
           resultCtrl <= "01"; --- Enable ALU in outputMux: 01		   
		elsif (codeCtrl(3 downto 2) = "10") then      -- Code control for the Shifter unit, enaling Shifter only from the output Mutiplexer : result = 10 
			shiftCtrl <= codeCtrl(1 downto 0);
			aluCtrl <= "000";    -- unknow values "-", not recomended
			lutEn <= '0';
		    resultCtrl <= "10";   --- Enable Shifter in outputMux : 10
		elsif (codeCtrl(2)='1') then      -- Code control for the Non-Linear Lookup unit, enabling only thi module :result = 00 
			lutEn <= '1';
			aluCtrl <= "000";
            shiftCtrl <= "00";
            resultCtrl <= "00"; --- Enable Non-Linear LT in outputMux : 00
     	else                           -- Ouput in multiplexer connected to GND 
    		aluCtrl <= "000";
            shiftCtrl <= "00";
            lutEn <= '0';
            resultCtrl <= "11";
		end if;
end process ctrl_Unit;
end rtl;
